(function($) {
    $(document).ready(function() {

		let $body = $('body');

		// mobile menu open/close functions
		$(function siteNavigation() {
			$('#menu-toggle').click(function() {
				$('.x-bar').toggleClass('x-bar-active');
				$('#primary-menu').fadeToggle(0);
				$('.mobile-nav-top').toggleClass('add-white-bg');
			});
		});

		// dropdown block - controls, events, and aria swap
		$(function dropDownBlock() {
			$('.dropdown-content').each(function() {
				$(this).hide();
			});

			$('.dropdown-block-title').click(function() {
				var $this = $(this);
				// fires on first click (content is expanded)
				if( $this.hasClass('target') ) {
					$this.removeClass('target');
					$this.next('.dropdown-content').slideToggle(700);
					$this.attr('aria-pressed', 'true');
					$this.next('.dropdown-content').attr('aria-expanded', 'true');
				} else {
					// fires on second click (content is closed)
					$this.next('.dropdown-content:first').slideToggle(700, function() {
						$this.prev('.dropdown-block-title').addClass('target');
						$this.prev('.dropdown-block-title').attr('aria-pressed', 'false');
						$this.attr('aria-expanded', 'false');
					});
				}
				// always fire
				$(this).toggleClass('chevron-rotate');
			});
		}); // end dropdown block

	}); // end Document.Ready
})(jQuery);