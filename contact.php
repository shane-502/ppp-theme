<?php
/**
 * Template Name: Contact
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.1
*/

$context = Timber::get_context();
$post = Timber::get_post();
$context['post'] = $post;

$context['contact_form'] = gravity_form( 1, false, false, false, '', false, 4, false );

$current = $post->ID;
$parent = $post->post_parent;
$grandparent_get = get_post($parent);
$grandparent = $grandparent_get->post_parent;

// check page hierarchy for parents and grandparent titles to show the parent title
if( $root_parent = get_the_title($grandparent) !== $root_parent = get_the_title($current) ) {
	$context['parent'] = get_the_title($grandparent);
} else {
	$context['parent'] = get_the_title($parent);
}

/**
	if the title obtained above is the current page (e.g. this page IS the parent) -
	overwrite parent context to show the site root as the parent
*/
if( get_the_title($parent) == get_the_title($current) ) {
	$context['parent'] = 'Purple Power Play';
}

$templates = [ 'contact.twig' ];

Timber::render( $templates, $context );