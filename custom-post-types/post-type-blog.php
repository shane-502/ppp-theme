<?php
// Setup the post type
$labels = [
	'name'               => __( 'Blogs', 'ppp' ),
	'singular_name'      => __( 'Blog', 'ppp' ),
	'add_new'            => _x( 'Add Blog', 'ppp', 'ppp' ),
	'add_new_item'       => __( 'Add Blog', 'ppp' ),
	'edit_item'          => __( 'Edit Blog', 'ppp' ),
	'new_item'           => __( 'New Blog', 'ppp' ),
	'view_item'          => __( 'View Blog', 'ppp' ),
	'search_items'       => __( 'Search Blogs', 'ppp' ),
	'not_found'          => __( 'No Blogs found', 'ppp' ),
	'not_found_in_trash' => __( 'No Blogs found in Trash', 'ppp' ),
	'parent_item_colon'  => __( 'Parent Blog:', 'ppp' ),
	'menu_name'          => __( 'Blogs', 'ppp' ),
];

$args = [
	'labels'              => $labels,
	'hierarchical'        => true,
	'description'         => '',
	'taxonomies'          => [],
	'public'              => true,
	'show_ui'             => true,
	'show_in_menu'        => true,
	'show_in_admin_bar'   => true,
	'show_in_rest'		  => false,
	'menu_icon'           => 'dashicons-calendar',
	'show_in_nav_menus'   => true,
	'publicly_queryable'  => true,
	'exclude_from_search' => false,
	'has_archive'         => true, // main blog listing
	'query_var'           => true,
	'can_export'          => true,
	'rewrite'             => true,
	'capability_type'     => 'post',
	'supports'            => [ 'editor', 'title', 'thumbnail' ],
];
register_post_type( 'blog', $args );